import { createApp } from 'vue'
import App from './App.vue'
import 'mdbvue/lib/css/mdb.min.css'
import '@fortawesome/fontawesome-free/css/all.min.css'
import 'bootstrap-css-only/css/bootstrap.min.css'
import router from './router'
import store from "./store";


const app = createApp(App)
app.use(store)
app.use(router)
app.mount('#app')



