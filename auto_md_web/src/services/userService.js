import axios from 'axios';
import CryptoJS from 'crypto-js';
import { User } from './user.js';

// axios.interceptors.request.use((config) => {
//     if (!config.url.includes('login') && !config.url.includes('register')) {
//         config.headers.token = localStorage.getItem('token');
//     }
//     return config;
// }, (error) => {
//     return Promise.reject(error);
// });


export async function getAllUsers() {
    const response = await axios.get('http://localhost:3000/api/users');
    return response.data;
}


export async function addUser(user) {
    user.password = encryptPassword(user.password);
    const response = await axios.post('http://localhost:3000/api/user?adduser=1', {}, { params: user });
    return response.data;
}

export async function deleteUser(id) {
    const response = await axios.post(`http://localhost:3000/api/user?deleteuser=1&&id=${id}`);
    return response.data;
}

export async function getUserById(id) {
    const response = await axios.get(`http://localhost:3000/api/user?id=${id}`);
    return response.data;
}

export async function getUserbyUsername(username) {
    const response = await axios.get(`http://localhost:3000/api/user?username=${username}`);
    return response.data;
}

export async function updateUserInDB(user, id) {
    user.password = encryptPassword(user.password);
    const response = await axios.post(`http://localhost:3000/api/user?updateuser=1&&id=${id}`, {}, { params: user });
    return response.data;
}

export function encryptPassword(password) {
    return CryptoJS.AES.encrypt(password, 'secret key 1').toString();
}

export async function login(username, password) {
    console.log(username, password);
    const response = await axios.post(`http://localhost:3000/api/login?username=${username}&&password=${password}`)
    if (response.status == 200) {
        localStorage.setItem('token', response.data.token);
        localStorage.setItem('uuid', response.data.uuid);
    }
    return response.data;
}

export async function register(user) {
    user.password = encryptPassword(user.password);
    const response = await axios.post(`http://localhost:3000/api/register`, {}, { params: user });
    if (response.status == 200) {
        localStorage.setItem('token', response.data.token);
        localStorage.setItem('uuid', response.data.uuid);
    }
    return response.data;
}


// register(new User("bebeww", "test", "test1@gmail.com")).then((response) => {
//     console.log(response);
// });




// getAllUsers().then((response) => {
//     console.log(response);
// });









