import { createStore } from 'vuex'
import { getUserById } from '../services/userService';

const store = createStore({
  state() {
    return {
      username: null,
      email: null,
    }
  },
  mutations: {
    setUsername(state, username) {
      state.username = username;
    },
    setEmail(state, email) {
      state.email = email;
    }
  },
  actions: {
    getUserInfo(context) {
      getUserById(localStorage.getItem('uuid')).then((response) => {
        context.commit('setEmail', response[0].email);
        context.commit('setUsername', response[0].username);
      }).catch((error) => {
        console.log(error);
      });
    },
    logout(context) {
      localStorage.removeItem('uuid');
      localStorage.removeItem('token');
      context.commit('setEmail', null);
      context.commit('setUsername', null);
    }

  }
})

export default store;