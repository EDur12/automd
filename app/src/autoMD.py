import argparse
import requests
import sys
import threading
import os
import subprocess
from tqdm import tqdm
import time
import getpass

# Define variables
file = ""
auto = False
gen = False
output_dir = ""
rename = ""
# create a session
session = requests.Session()

# Parse command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("-f", help="Specify the input file for autoMD.py")
parser.add_argument("--login", action="store_true", help="Enable login mode", default=False)
parser.add_argument("--logout", action="store_true", help="Enable logout mode", default=False)
parser.add_argument("--register", action="store_true", help="Enable register mode", default=False)
parser.add_argument("-a", action="store_true", help="Automatically generate the input file with history commands and run autoMD.py (Linux)", default=False)
parser.add_argument("-g", action="store_true", help="Generate the input file with history commands but do not run autoMD.py", default=False)
parser.add_argument("-o", help="Specify the output directory for the generated Markdown files")
parser.add_argument("-r", help="Rename the output Markdown file")
args = parser.parse_args()

# Set the input file path
file = args.f

# if login mode is enabled
if args.login:
    # get username and password
    print("Login mode enabled")
    username = input("Username: ")
    password = getpass.getpass("Password: ")

    # send login request
    response = session.post(f"https://automdapi.arpanode.fr/login?username={username}&password={password}")
    if response.status_code == 200:
        print("Logged as " + username)
        # ask for store username and password for auto login if login.txt does not exist
        if not os.path.exists("login.txt"):
            store = input("Store username for auto login? (y/n): ")
            if store == "y":
                # store username
                with open("login.txt", "w") as f:
                    f.write(username)
                print("Username stored")
            sys.exit(0)
        sys.exit(0)
    else:
        print("Login failed")
        sys.exit(1)

# if logout mode is enabled
if args.logout:
    # send logout request
    response = requests.get("https://automdapi.arpanode.fr/logout")
    print("Logout request sent")
    if response.status_code == 200:
        print("Logout successful")
    else:
        print("Logout failed")
        sys.exit(1)

# if register mode is enabled
if args.register:
    # get username and password
    print("Register mode enabled")
    username = input("Username: ")
    password = getpass.getpass("Password: ")
    password_verif = getpass.getpass("Password verification: ")
    api_key = input("OpenAI API key: ")

    # check if password and password verification are the same
    if password == password_verif:
        # send register request
        response = requests.post(f"https://automdapi.arpanode.fr/register?name={username}&password={password}&api_key={api_key}")
        print("Register request sent")
        if response.status_code == 200:
            print("Register successful")
            print("Now you can login with your credentials")
            # ask for store username and password for auto login if login.txt does not exist
            if not os.path.exists("login.txt"):
                store = input("Store username for auto login? (y/n): ")
                if store == "y":
                    # store username
                    with open("login.txt", "w") as f:
                        f.write(username)
                    print("Username stored")
                sys.exit(0)
            sys.exit(0)
        else:
            print("Register failed")
            sys.exit(1)
    else :
        print("Password and password verification are not the same")
        sys.exit(1)

# Enable automatic mode
if args.a:
    # check if Linux is the OS
    if sys.platform == "linux":
        # Generate input file with history commands
        subprocess.run(["history", "-w", "commande.txt"])

        # Set the input file path
        file = "./history.txt"
        auto = True
    else:
        print("Error: automatic mode is only available on Linux", file=sys.stderr)
        sys.exit(1)

# Enable generation mode
if args.g:
    # check if Linux is the OS
    if sys.platform == "linux":
        # Generate input file with history commands
        subprocess.run(["history", "-w", "commande.txt"])
        # Set the input file path
        file = "./history.txt"
        gen = True

    else:
        print("Error: generation mode is only available on Linux", file=sys.stderr)
        sys.exit(1)

# Check if generation mode is enabled and output directory path is set generate input file with history commands at the output directory
if args.g and args.o:
    # check if Linux is the OS
    if sys.platform == "linux":
        # Generate input file with history commands
        subprocess.run(["history", "-w", os.path.join(args.o, "history.txt")])
        # Set the input file path
        file = os.path.join(args.o, "history.txt")
        gen = True

    else:
        print("Error: generation mode is only available on Linux", file=sys.stderr)
        sys.exit(1)

# Check if the input file path is set
if not file and not (auto or gen):
    # Display error message and exit
    print("Error: -f option is required when automatic or generation mode is not enabled", file=sys.stderr)
    sys.exit(1)

# Check if the input file exists
if file and not os.path.isfile(file):
    # Display error message and exit
    print(f"Error: input file {file} does not exist", file=sys.stderr)
    sys.exit(1)

# Check if the output directory path is set
if args.o and not os.path.isdir(args.o):
    # Create the output directory
    os.makedirs(args.o)

file_path = args.f
url = "https://automdapi.arpanode.fr/generate_markdown"
files = {"file": open(file_path, "rb")}
fileName = os.path.splitext(os.path.basename(file_path))[0]

output_dir = args.o if args.o else "."
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

if args.r:
    fileName = args.r

# Start the spinner
def progressBar():
    with tqdm(total=100, desc="Generating Markdown", unit="%", ncols=80) as pbar:
        while True:
            pbar.update(1)
            time.sleep(0.5)
            if pbar.n == 100:
                break

response = None

# Check if the input file is a text file
if not file_path.endswith(".txt"):
    print("Error: input file must be a text file", file=sys.stderr)

# login to the server
# get username and password
print("Please login to Automd API to generate markdown")
print("(If you don't have an account, please register with the --register option)\n")

# check if login.txt exists
if os.path.isfile("login.txt"):
    with open("login.txt", "r") as f:
        username = f.readline().strip()
    # send login request
    password = getpass.getpass("Password: ")
    response = session.post(f"https://automdapi.arpanode.fr/login?username={username}&password={password}")
    if response.status_code == 200:
        print("Logged as " + username)
    else:
        print("Login failed")
        sys.exit(1)
else:
    username = input("Username: ")
    password = getpass.getpass("Password: ")
    # send login request
    response = session.post(f"https://automdapi.arpanode.fr/login?username={username}&password={password}")
    if response.status_code == 200:
        print("Logged as " + username)
        # ask for store username and password for auto login if login.txt does not exist
        if not os.path.exists("login.txt"):
            store = input("Store username for auto login? (y/n): ")
            if store == "y":
                # store username
                with open("login.txt", "w") as f:
                    f.write(username)
                print("Username stored")
            sys.exit(0)
    else :
        print("Login failed")
        sys.exit(1)

time.sleep(1)
# press enter to continue
input("Press enter for generate your markdown file")
# Start the spinner in a separate thread
spinner_thread = threading.Thread(target=progressBar)
spinner_thread.start()

# Send the request
response = session.post(url, files=files)

# Stop the spinner
spinner_thread.join()

if response.status_code == 200:
    # Save the response to a file in the output directory
    output_path = os.path.join(output_dir, f"{fileName}.md")
    with open(output_path, "w") as f:
        f.write(response.text)
    print(f"Markdown file generated at {output_path}")
    # Logout
    response = session.get("https://automdapi.arpanode.fr/logout")
    if response.status_code != 200:
        print("Logout failed")
        sys.exit(1)

else:
    print(f"Error: {response.status_code} {response.reason}", file=sys.stderr)
    sys.exit(1)

if auto:
    # Remove the input file
    os.remove(file)