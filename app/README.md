# AutoMD.py

AutoMD.py is a Python script that generates Markdown documentation from a text file containing history commands. It utilizes the Automd API to convert the commands into formatted Markdown.

## Prerequisites

- Python 3.x
- `argparse` library
- `requests` library
- `tqdm` library

## Installation

1. Clone the repository `https://gitlab.com/apajak/automd_app.git`.

2. Install the required libraries by running the following command:

   ```bash
   pip install -r requirements.txt
   ```

## Usage

Run the `AutoMD.py` script with the following command-line options:

```bash
python AutoMD.py [-h] [-f INPUT_FILE] [--login] [--logout] [--register] [-a] [-g] [-o OUTPUT_DIR] [-r RENAME]
```

The available options are:

- `-h`, `--help`: Show the help message and exit.
- `-f INPUT_FILE`: Specify the input file for `AutoMD.py`.
- `--login`: Enable login mode.
- `--logout`: Enable logout mode.
- `--register`: Enable register mode.
- `-a`: Automatically generate the input file with history commands and run `AutoMD.py`.
- `-g`: Generate the input file with history commands but do not run `AutoMD.py`.
- `-o OUTPUT_DIR`: Specify the output directory for the generated Markdown files.
- `-r RENAME`: Rename the output Markdown file.

### Login Mode

To use the login mode, specify the `--login` option. You will be prompted to enter your username and password. The script will send a login request to the Automd API using the provided credentials.

```bash
python AutoMD.py --login
```

### Logout Mode

To use the logout mode, specify the `--logout` option. The script will send a logout request to the Automd API.

```bash
python AutoMD.py --logout
```

### Register Mode

To use the register mode, specify the `--register` option. You will be prompted to enter your desired username, password, and OpenAI API key. The script will send a register request to the Automd API with the provided information.

```bash
python AutoMD.py --register
```
Test api key for Ynov staff:`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`

### Automatic Mode

To enable automatic mode, use the `-a` option. The script will generate an input file with history commands using the `history` command and then run `AutoMD.py` with the generated file.

```bash
python AutoMD.py -a
```

### Generation Mode

To enable generation mode, use the `-g` option. The script will generate an input file with history commands using the `history` command but will not run `AutoMD.py`.

```bash
python AutoMD.py -g
```

### Input File

To specify an input file for `AutoMD.py`, use the `-f` option followed by the file path.

```bash
python AutoMD.py -f input.txt
```

### Output Directory

To specify an output directory for the generated Markdown files, use the `-o` option followed by the directory path. If the directory does not exist, it will be created.

```bash
python AutoMD.py -o output_dir
```

### Rename Output File

To rename the output Markdown file, use the `-r` option followed by the new file name.

```bash
python AutoMD.py -r new_file_name
```