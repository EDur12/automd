const express = require('express')
const mysql = require('mysql2');
const CryptoJS = require('crypto-js');
const jwt = require('jsonwebtoken');
var session = require('express-session')
var cookieParser = require('cookie-parser');
const Cors = require("cors");
const app = express()
const port = 3000

const connection = mysql.createConnection({
    host: '51.77.137.116',
    user: 'autoMD',
    password: 'S7r0nGpw8',
    database: 'autoMD',
    port: 3306
});

function isLogged(req, res, next) {
    if (req.headers.token == req.session.token) {
        next();
    } else {
        res.status(401).json({
            sucess: false,
        });
    }
}

app.use(Cors({ origin: "*", credentials: true }));
app.use(cookieParser());
app.use(session(
    {

        secret: "Shh, its a secret!",
        //cookie: { secure: false, maxAge: 60000 },
        resave: false,
        saveUninitialized: true,
    }
));

app.get('/api/users', isLogged, (req, res) => {
    connection.query('SELECT * FROM user', (error, results) => {
        if (error) {
            throw error;
        }
        res.json(results);
    });
})



app.post('/api/user', isLogged, (req, res) => {
    if (req.query.deleteuser == 1) {
        const uuid = req.query.id;
        const query = 'DELETE FROM user WHERE uuid = ?';
        connection.query(query, [uuid], (err, results, fields) => {
            if (err) {
                throw err;
            }
            console.log('Deleting user:::::');
            res.json(results);
        });
    }
    else if (req.query.updateuser == 1) {
        const uuid = req.query.id;
        const username = req.query.username;
        const email = req.query.email;
        const password = req.query.password;
        const query = 'UPDATE user SET username = ?, email = ?, passwd = ? WHERE uuid = ?';
        connection.query(query, [username, email, password, uuid], (err, results, fields) => {
            if (err) {
                throw err;
            }
            console.log('Updating user:::::');
            res.json(results);
        });
    }


});

app.get('/api/user', function (req, res) {
    if (req.query.id) {
        const id = req.query.id;
        connection.query('SELECT * FROM user WHERE uuid = ?', [id], (error, results) => {
            if (error) {
                throw error;
            }
            res.json(results);
        });
    }
    else if (req.query.username) {
        const username = req.query.username;
        connection.query('SELECT * FROM user WHERE username = ?', [username], (error, results) => {
            if (error) {
                throw error;
            }
            res.json(results);
        });
    }
});

app.post('/api/login', isLogged, (req, res) => {
    const username = req.query.username;
    const password = req.query.password;
    connection.query('SELECT * FROM user WHERE username = ?', [username], (error, results) => {
        if (error) {
            throw error;
        }
        const user = results[0];
        console.log(user);
        if (decryptPassword(user.passwd) == password) {
            var token = jwt.sign(
                { id: user.uuid },
                'RANDOM_TOKEN_SECRET',
                { expiresIn: '24h' }
            )
            req.session.uuid = user.uuid;
            req.session.token = token;
            res.status(200).json({
                sucess: true,
                token: token,
                uuid: user.uuid,
            })
        } else {
            res.status(401).json({
                error: new Error('Invalid request!')
            });
        }
    });
})

app.post('/api/register', isLogged, (req, res) => {
    const username = req.params.username;
    const email = req.params.email;
    const password = req.params.password;
    const query = 'INSERT INTO user (username, email, passwd) VALUES (?, ?, ?)';
    connection.query(query, [username, email, password], (error, results, fields) => {
        if (error) {
            throw error;
        }
        var token = jwt.sign(
            { id: user.uuid },
            'RANDOM_TOKEN_SECRET',
            { expiresIn: '24h' }
        )
        req.session.uuid = user.uuid;
        req.session.token = token;
        res.status(200).json({
            sucess: true,
            token: token,
            uuid: user.uuid,
        })
    });

})

app.get('/api/test', isLogged, (req, res) => {
    res.status(200).json({
        sucess: true,
    })
})

// Configure CORS for all routes
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', 'http://automd.arpanode.fr');
    res.header('Access-Control-Allow-Origin', 'https://automd.arpanode.fr');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    next();
  });

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})


function decryptPassword(password) {
    var bytes = CryptoJS.AES.decrypt(password, 'secret key 1');
    return bytes.toString(CryptoJS.enc.Utf8);
}

