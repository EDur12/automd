import mysql.connector
from mysql.connector import errorcode
import bcrypt
import datetime
from usermanager import User

class MySQLManager:
    def __init__(self, host, user, password, database):
        self.host = host
        self.user = user
        self.password = password
        self.database = database
        self.cnx = None
        self.cursor = None

    def connect(self):
        try:
            self.cnx = mysql.connector.connect(host=self.host,
                                               user=self.user,
                                               password=self.password,
                                               database=self.database,
                                               autocommit=True)
            self.cursor = self.cnx.cursor()
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)

    def close(self):
        self.cursor.close()
        self.cnx.close()

    def execute(self, query, args=None):
        if args is None:
            self.cursor.execute(query)
        else:
            self.cursor.execute(query, args)

    def fetchall(self):
        return self.cursor.fetchall()

    def fetchone(self):
        return self.cursor.fetchone()

    def get_last_row_id(self):
        return self.cursor.lastrowid

    def get_row_count(self):
        return self.cursor.rowcount

    def insert_user(self, user):
        self.connect()
        user.password = bcrypt.hashpw(user.password, bcrypt.gensalt())
        try:
            self.execute("INSERT INTO User (uuid, username, password, apiKey, session)VALUES (%s, %s, %s, %s, %s)",
                         (str(user.uuid), user.username, user.password, user.apiKey, user.session))
            self.close()
            return True
        except mysql.connector.Error as err:
            print(err)
            self.close()
            return False

    def update_user(self, user):
        self.connect()
        try:
            self.execute("UPDATE User SET username = %s, password = %s, apiKey = %s, session = %s WHERE uuid = %s",
                         (user.username, user.password, user.apiKey, user.session, str(user.uuid)))
            self.close()
            return True
        except mysql.connector.Error as err:
            print(err)
            self.close()
            return False

    def delete_user(self, user):
        self.connect()
        try:
            self.execute("DELETE FROM User WHERE uuid = %s", (str(user.uuid),))
            self.close()
            return True
        except mysql.connector.Error as err:
            print(err)
            self.close()
            return False



    def get_user(self, uuid):
        self.connect()
        self.execute("SELECT * FROM User WHERE uuid = %s", (str(uuid),))
        tuple_user = self.fetchone()
        print(tuple_user)
        user = User(tuple_user[2], tuple_user[3], tuple_user[4], tuple_user[1])
        self.close()
        return user

    def get_user_by_username(self, username):
        self.connect()
        self.execute("SELECT * FROM User WHERE username = %s", (username,))
        tuple_user = self.fetchone()
        if tuple_user is None:
            return None
        else:
            user = User(tuple_user[2], tuple_user[3], tuple_user[4], tuple_user[1])
            self.close()
            return user

    def get_user_apiKey(self, uuid):
        self.connect()
        self.execute("SELECT apiKey FROM User WHERE uuid = %s", (str(uuid),))
        apiKey = self.fetchone()
        self.close()
        return apiKey




    def login(self, username, password):
        self.connect()
        self.execute("SELECT * FROM User WHERE username = %s", (username,))
        tuple_user = self.fetchone()
        user = User(tuple_user[2], tuple_user[3], tuple_user[4], tuple_user[1])
        if bcrypt.checkpw(password.encode(), user.password.encode()):
            user.session = str(datetime.datetime.now())
            self.update_user(user)
            self.close()
            return user
        else:
            self.close()
            return None
