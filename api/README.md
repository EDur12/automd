# Automd FastAPI Service

The Automd FastAPI Service is an API that provides functionality for user registration, login, session management, and markdown generation.

## How it Works

The API is built using [FastAPI](https://fastapi.tiangolo.com/), a modern, fast (high-performance), web framework for building APIs with Python 3.7+ based on standard Python type hints. It utilizes various libraries and modules for different functionalities.

The API provides the following endpoints:

### User Registration

Endpoint: `POST /register`

This endpoint allows users to register by providing their name, password, and API key. Upon successful registration, the user's information is stored in a MySQL database.

### User Login

Endpoint: `POST /login`

Users can log in to the API using their username and password. If the credentials are valid, a session is created for the user, and a session cookie is attached to the response. This endpoint utilizes session management with UUID-based session identifiers and in-memory storage.

### User Logout

Endpoint: `GET /logout`

Logging out from the API is done by sending a GET request to this endpoint. It deletes the user's session from the session backend and removes the session cookie from the response.

### Generate Markdown

Endpoint: `POST /generate_markdown`

This endpoint allows authenticated users to generate a markdown file. Users need to provide an uploaded file, and the API processes it using the `MDai` class from the external `mdai` module. The generated markdown file is returned as a response.

### User Session Information

Endpoint: `GET /whoami`

This endpoint provides information about the current user's session. It requires authentication and returns the session data, including the UUID of the user.

## Setup and Running the API

To set up and run the Automd FastAPI Service, follow these steps:

1. Install the required dependencies. You can use a virtual environment to isolate the dependencies if desired.
   ```shell
   pip install -r requirements.txt
    ```
2. Configure the MySQL database connection details in the MySQLManager class initialization within the code.
3. Run the API using the following command:
   ```shell
   uvicorn main:app --host 127.0.0.1 --port 8001
   ```
   This starts the API server on http://127.0.0.1:8001/.
## External Modules

The API relies on the following external modules:

- verifier: Provides the BasicVerifier class for session verification.
- databaseManager: Contains the MySQLManager class for managing the MySQL database.
- usermanager: Includes the User class for representing a user.
- mdai: Provides the MDai class for interacting with an external API.
Please ensure that these modules are available and properly installed for the API to function correctly.

## Deployment
### Systemd service configuration:
```bash
[Unit]
Description=Automd FastAPI Service
After=network.target

[Service]
User=lexit
Group=lexit
WorkingDirectory=/home/lexit/automd/api
Environment="PATH=/home/lexit/.virtualenvs/automd/bin"
ExecStart=/home/lexit/.virtualenvs/automd/bin/python3 main.py
Restart=always

[Install]
WantedBy=multi-user.target
```

### NGINX revers proxy configuration:
```bash
server {
    server_name www.automdapi.arpanode.fr automdapi.arpanode.fr;

    location / {
        proxy_pass http://127.0.0.1:8001;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/automdapi.arpanode.fr/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/automdapi.arpanode.fr/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}

server {
    if ($host = automdapi.arpanode.fr) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    listen 80;
    server_name www.automdApi.arpanode.fr automdapi.arpanode.fr;
    return 404; # managed by Certbot


}
```