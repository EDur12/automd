from pydantic import BaseModel
from fastapi import HTTPException, FastAPI, Response, Depends
from fastapi.responses import FileResponse
from fastapi import File, UploadFile
from uuid import UUID, uuid4
from verifier import BasicVerifier
import os
import bcrypt
from databaseManager import MySQLManager
from usermanager import User

from mdai.mdai_v3 import MDai
from fastapi_sessions.backends.implementations import InMemoryBackend
from fastapi_sessions.frontends.implementations import SessionCookie, CookieParameters


class SessionData(BaseModel):
    uuid: str


cookie_params = CookieParameters()

# Uses UUID
cookie = SessionCookie(
    cookie_name="session",
    identifier="general_verifier",
    auto_error=True,
    secret_key="DONOTUSE",
    cookie_params=cookie_params,
)
backend = InMemoryBackend[UUID, SessionData]()

verifier = BasicVerifier(
    identifier="general_verifier",
    auto_error=True,
    backend=backend,
    auth_http_exception=HTTPException(status_code=403, detail="invalid session"),
)

app = FastAPI()
db = MySQLManager("54.37.153.179","automd","97aXRAe5M#F6o8t","automd")

def verify_password(plain_password, hashed_password):
    return bcrypt.checkpw(plain_password.encode(), hashed_password.encode())
@app.post("/login")
async def create_session(username: str, password: str, response: Response):
    user = db.get_user_by_username(username)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")

    if not verify_password(password, user.password):
        raise HTTPException(status_code=401, detail="Incorrect password")

    session = uuid4()
    data = SessionData(uuid=user.uuid)

    await backend.create(session, data)
    cookie.attach_to_response(response, session)
    print(f"{user.username} logged in successfully")
    return {"message": f"{user.username} logged in successfully"}

@app.post("/register")
async def create_user(name: str, password: str,api_key:str):
    user = db.get_user_by_username(name)
    if user:
        raise HTTPException(status_code=409, detail="User already exists")
    else:
        try :
            hashed_password = bcrypt.hashpw(password.encode(), bcrypt.gensalt())
            new_user = User(name, hashed_password, api_key)
            db.insert_user(new_user)
            print(f"{new_user.username} registered successfully")
            return {"message": f"{new_user.username} registered successfully"}
        except Exception as e:
            raise HTTPException(status_code=500, detail="Internal server error")


@app.get("/whoami", dependencies=[Depends(cookie)])
async def whoami(session_data: SessionData = Depends(verifier)):
    return session_data


@app.get("/logout")
async def del_session(response: Response, session_id: UUID = Depends(cookie)):
    await backend.delete(session_id)
    cookie.delete_from_response(response)
    print("deleted session")
    return "deleted session"

@app.post("/generate_markdown",dependencies=[Depends(cookie)])
async def generate_markdown(
    file: UploadFile = File(...),
    session_data: SessionData = Depends(verifier),
):
    # Check if user is logged in
    if not session_data:
        raise HTTPException(status_code=401, detail="Unauthorized. Please log in.")
    # get user from session
    user = db.get_user(session_data.uuid)
    mdai = MDai(user.apiKey)

    try:
        # Save the uploaded file
        file_location = os.path.join("./files", file.filename)
        with open(file_location, "wb") as buffer:
            buffer.write(file.file.read())

        # Generate the markdown file
        print("Generating markdown")
        output_file = mdai.mdGenerator(file_location)
        print("Markdown generated")

        # Return the generated markdown file
        return FileResponse(output_file)

    except Exception as e:
        # Handle specific exceptions if needed
        if isinstance(e, FileNotFoundError):
            raise HTTPException(status_code=404, detail="File not found.")
        else:
            raise HTTPException(status_code=500, detail="Internal server error. {}".format(e))


if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="127.0.0.1", port=8001)