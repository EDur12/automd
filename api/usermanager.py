from uuid import uuid4
class User:
    def __init__(self,username, password, apiKey, uuid=uuid4()):
        self.id = None
        self.uuid = uuid
        self.username = username
        self.password = password
        self.apiKey = apiKey
        self.session = None

    def __str__(self):
        return f"User: {self.username}, {self.password}, {self.apiKey}, {self.session}"

    def user_to_tuple(self):
        return (str(self.uuid), self.username, self.password, self.apiKey, self.session)

    def tuple_to_user(self, user):
        self.uuid = user[0]
        self.username = user[1]
        self.password = user[2]
        self.apiKey = user[3]
        self.session = user[4]
        return self