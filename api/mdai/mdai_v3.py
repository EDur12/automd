import os
import time
from dotenv import load_dotenv
from transformers import GPT2Tokenizer
import openai
import re


class MDai:
    def __init__(self, api_key):
        self.api_key = api_key
        self.request_count = 0
        self.model = "text-davinci-003"
        self.prompt = \
            "\n\nGroup these commands by context and write a markdown report of the related installation in markdown format."
        self.MAX_REQUESTS_PER_MINUTE = 60
        self.DEFAULT_chunk_size = 2000  # more than 2000 chars will cause an shorter response
        self.temperature = 0
        self.top_p = 1
        self.frequency_penalty = 0
        self.presence_penalty = 0
        self.stop = ["\"\"\""]

    def _createFile(self, file_path):
        """Create output file"""
        # Restrict file types and extensions if necessary
        if not file_path.endswith(".txt"):
            raise ValueError("Invalid file type. Only .txt files are allowed.")
        # Convert file path to absolute path
        file_dirname = os.path.dirname(file_path)
        file_basename = os.path.basename(file_path)
        output_dir = os.path.join(file_dirname, "markdown")
        output_file = os.path.splitext(file_basename)[0] + ".md"
        output_path = os.path.join(output_dir, output_file)

        try:
            # Create output directory if it doesn't exist
            os.makedirs(output_dir, exist_ok=True)

            # Create the output file
            open(output_path, "w").close()
            return output_path

        except Exception as e:
            # Handle file creation errors
            raise ValueError("Failed to create file: {}".format(str(e)))

    def _cleanUpFile(self, string):
        """clean file before processing"""
        # Remove non-ASCII characters
        cleaned_string = string.encode('ascii', 'ignore').decode()
        # Remove numbers at the start of each line
        cleaned_string = re.sub(r"^\s*\d+\s*", "", cleaned_string, flags=re.MULTILINE)
        return cleaned_string

    def _getChunks(self, file):
        """divide file on array of 'MAX_chunk_size'"""
        fileContent = self._read(file)
        chunks = []
        for i in range(0, len(fileContent), self.DEFAULT_chunk_size):
            chunks.append(fileContent[i:i + self.DEFAULT_chunk_size])
        return chunks

    def _read(self, file):
        """read input file"""
        with open(file, "r") as f:
            return self._cleanUpFile(f.read())

    def _write(self, file, content):
        """write in output file"""
        with open(file, "a") as f:
            f.write(content)

    def _close(self, file):
        """close file"""
        file.close()

    def _getMaxTokens(self, chunk):
        """Get the maximum number of tokens for each chunk"""
        MAX_tokens = 4096
        margin = 50
        try:
            # Initialize tokenizer
            tokenizer = GPT2Tokenizer.from_pretrained("gpt2")
            # Tokenize the chunk
            chunk_tokens = tokenizer.encode(chunk, add_special_tokens=False)
            # Calculate the number of tokens
            num_tokens = len(chunk_tokens)
            print("chunk_tokens:", num_tokens)
            return MAX_tokens - (num_tokens + margin)
        except Exception as e:
            print("Tokenization error:", str(e))
            return 0

    def _request(self, chunk, max_tokens):
        """send request to openai api"""
        try:
            openai.api_key = self.api_key
            response = openai.Completion.create(
                model=self.model,
                prompt=f"{chunk}{self.prompt}",
                temperature=self.temperature,
                max_tokens=max_tokens,
                top_p=self.top_p,
                frequency_penalty=self.frequency_penalty,
                presence_penalty=self.presence_penalty,
            )
            return response.choices[0].text
        except Exception as e:
            return f"Request error:{str(e)}"

    def mdGenerator(self, file):
        """Main function"""
        try:
            chunks = self._getChunks(file)
            output_file = self._createFile(file)
            for chunk in chunks:
                max_tokens = self._getMaxTokens(chunk)
                if max_tokens > 0:
                    response = self._request(chunk, max_tokens)
                    self._write(output_file, response)
                else:
                    print("Skipping chunk due to tokenization error.")
            return output_file

        except FileNotFoundError:
            print("File not found error.")
        except Exception as e:
            print("Error:", str(e))